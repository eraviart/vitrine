# Vitrine

_Web site displaying pages & data retrieved dynamically from other sites_

This project has migrated to https://git.en-root.org/eraviart/vitrine.

[Demo](https://eraviart.frama.io/vitrine/)

![Screenshot of a web site based on Vitrine](https://forum.parlement-ouvert.fr/uploads/default/optimized/1X/a0a1d5850054eadca16d05a019b5165e08ed9c87_1_623x500.jpg)

_Vitrine_ is an application that generates dynamically the landing pages of a web site using documents & data retrieved from other sites (using their APIs).

It can currently use pages from:
* [Discourse](https://www.discourse.org/) forums
* [GitLab](https://about.gitlab.com/) projects & groups

It can be used as the home page for a hackathon, to display the projects, the datasets, the participants, etc.

_Vitrine_ started as a clone of [Hackdash](https://hackdash.org/), but instead of having its own back-office, it uses Discourse and benefits from it power and versatility.

Usages examples:

* [#dataFin Hackathon 2018 & 2020](https://datafin.fr/)
* [AGDIC Hackathon 2019](https://agdic-hackathon.org/)

_Vitrine_ is free and open source software.

* [software repository](https://framagit.org/eraviart/vitrine)
* [GNU Affero General Public License version 3 or greater](https://framagit.org/eraviart/vitrine/blob/master/LICENSE.md)

## Installation

```bash
git clone https://framagit.org/eraviart/vitrine.git
cd vitrine/
npm install
```

## Customization

The easiest way to customize Vitrine is to fork this repository and edit `content/config.yml` and the other files in this directory.

Other files under `src/` can be modified too, but require more development skills.

If you enable GitLab pages in your fork, your fork will be deployed automatically under `https://<username>.frama.io/vitrine/`

## Development

```bash
npm run dev
```

If you change `content/config.yml` or any file in `src/`, the application will reload.

### Production

To build the application for a production use:

```bash
npm run export
```

This will generate a `__sapper__/export/` directory containing the static website.

See also `.gitlab-ci.yml` for an example.

## Discourse Configuration

Discourse must be configured to accept requests coming from another site (ie CORS must be enabled to accept requests coming from Vitrine web site).

In file `/var/discourse/containers/app.yml`, add line:
```yaml
  env:
    [...]
    DISCOURSE_ENABLE_CORS: true
```

Then in Discourse admin web page `/admin/site_settings/category/security` fill field **cors origin** with the URL of the Vitrine web site.
