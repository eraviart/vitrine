const mode = process.env.NODE_ENV
const dev = mode === "development"

module.exports = {
  plugins: [
    require("postcss-import")(),
    require("postcss-url")(),
    require("tailwindcss"),
    require("postcss-preset-env")({
      stage: 1,
      features: {
        "nesting-rules": true
      }
    }),
    ...(!dev
      ? [
          require("cssnano")({
            preset: "default"
          }),
          require("@fullhuman/postcss-purgecss")({
            content: ["./src/**/*.html", "./src/**/*.svelte"],

            defaultExtractor: content => {
              const fromClasses = content.match(/class:[A-Za-z0-9-_]+/g) || []

              return [
                ...(content.match(/[A-Za-z0-9-_:/]+/g) || []),
                ...fromClasses.map(c => c.replace("class:", ""))
              ]
            },

            whitelist: [
              // Vitrine specific
              "bg-white-trans",
              "stroke-primary",
            ],

            whitelistPatterns: [
              /svelte-/,

              // Vitrine specific
              /ripple/
            ]
          })
        ]
      : [])
  ]
}
