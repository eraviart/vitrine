import * as sapper from "@sapper/server"

import compression from "compression"
import fs from "fs"
import path from "path"
import polka from "polka"
import sirv from "sirv"
import yaml from "js-yaml"

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"

export const CONTENT_DIR = "content"
const config = yaml.safeLoad(fs.readFileSync(path.resolve(CONTENT_DIR, "config.yml"), "utf8"))
if (process.env.VITRINE_BASE_PATH) {
	config.basePath = process.env.VITRINE_BASE_PATH
}
config.basePath = config.basePath || "/"
// TODO Validate config

console.log("config", JSON.stringify(config, null, 2))


polka() // You can also use Express
	.use(
		config.basePath,
		compression({ threshold: 0 }),
		sirv("static", { dev }),
		sapper.middleware({
			session: () => ({ config }),
		})
	)
	.listen(PORT, err => {
		if (err) console.log("error", err)
	})
