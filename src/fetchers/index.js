export * from "./errors.js"

import * as discourse from "./discourse.js"
import * as gitlab from "./gitlab.js"

import { ConfigError } from "./errors.js"

const fetchers = { discourse, gitlab }

export function loadFetcher(sourceType) {
  const fetcher = fetchers[sourceType]
  if (!fetcher) {
    throw new ConfigError({
      message: `Source type "${sourceType}" not supported`,
      info: { sourceType },
    })
  }
  return fetcher
}

export async function fetchPageData(pageType, fetcher, source, options) {
  if (pageType === "cards") {
    return await fetcher.fetchCardsPageData.bind(this)(source, options)
  } else if (pageType === "content") {
    return await fetcher.fetchContentPageData.bind(this)(source)
  }
}
