import { ConfigError, FetchError } from "./errors.js"

export function getPageType(source) {
  if (source.category) {
    return "cards"
  } else if (source.topic) {
    return "content"
  } else {
    throw new ConfigError({
      message: `Missing "category" or "topic" property in "${source.type}" source`,
      info: { source },
    })
  }
}

export async function fetchCardsPageData(source, options) {
  const topicsResults = await fetchCategoryTopics.bind(this)(source, options)
  if (topicsResults && topicsResults.topic_list && topicsResults.topic_list.topics) {
    return topicsResults.topic_list.topics.map(topic => topicToCardData(source, topic))
  }
}

export async function fetchContentPageData(source) {
  return topicToContentData(await fetchTopic.bind(this)(source))
}

export function topicToCardData(source, topic) {
  return {
    counts: [
      {
        count: topic.like_count,
        icon: "favorite", // Cf https://material.io/resources/icons
        title: "Likes",
      },
      {
        count: topic.reply_count,
        icon: "reply", // Cf https://material.io/resources/icons
        title: "Replies",
      },
    ],
    id: topic.id,
    imageUrl: topic.image_url,
    html: topic.excerpt,
    title: topic.title,
    url: `${source.url}/t/${topic.slug}`,
  }
}

export function topicToContentData(topic) {
  const firstPost = topic.post_stream.posts[0]
  return {
    html: firstPost.cooked,
    title: topic.title,
  }
}

export async function fetchCategoryTopics(source, options = {}) {
  const { url: baseUrl, category } = source
  const { limit = null } = options
  const url = `${baseUrl}/c/${category}.json`
  let result = null
  for (let page = 0; ; page++) {
    const pageUrl = `${url}?page=${page}`
    const info = { url, pageUrl, source, options }
    let response
    try {
      response = await this.fetch(pageUrl)
      if (!response.ok) {
        throw response
      }
    } catch (error) {
      throw new FetchError({
        message: `Error while fetching Discourse category "${category}"`,
        info,
      }).withCause(error)
    }
    let data
    try {
      data = await response.json()
    } catch (error) {
      throw new FetchError({
        message: `Error while parsing JSON of Discourse category "${category}"`,
        info,
      }).withCause(error)
    }
    if (result === null) {
      result = data
    } else {
      result.topic_list.topics = [
        ...result.topic_list.topics, // already fetched topics
        ...data.topic_list.topics, // newly fetched topics
      ]
    }
    if (limit !== null && data.topic_list.topics.length >= limit) {
      result.topic_list.topics = result.topic_list.topics.slice(0, limit)
      return result
    } else if (data.topic_list.topics.length < data.topic_list.per_page) {
      return result
    }
  }
}

export async function fetchTopic(source) {
  const { url: baseUrl, topic } = source
  const url = `${baseUrl}/t/${topic}.json`
  const info = { url, source }
  let response
  try {
    response = await this.fetch(url)
    if (!response.ok) {
      throw response
    }
  } catch (error) {
    throw new FetchError({
      message: `Error while fetching Discourse topic "${topic}"`,
      info,
    }).withCause(error)
  }
  try {
    return await response.json()
  } catch (error) {
    throw new FetchError({
      message: `Error while parsing JSON of Discourse topic "${topic}"`,
      info,
    }).withCause(error)
  }
}
