import { ConfigError, FetchError } from "./errors.js"

export function getPageType(source) {
  if (source.group) {
    return "cards"
  } else if (source.project) {
    return "content"
  } else {
    throw new ConfigError({
      message: `Missing "group" or "project" property in "${source.type}" source`,
      info: { source },
    })
  }
}

export async function fetchCardsPageData(source, options) {
  return (await fetchGroupProjects.bind(this)(source, options)).map(projectToCardData)
}

export async function fetchContentPageData(source) {
  throw new Error("Not implemented: should return data for a content page, based on the README")
}

export function projectToCardData(project) {
  return {
    counts: [
      {
        count: project.star_count,
        icon: "star", // Cf https://material.io/resources/icons
        title: "Stars",
      },
      {
        count: project.forks_count,
        icon: "call_split", // Cf https://material.io/resources/icons
        title: "Forks",
      },
    ],
    id: project.id,
    imageUrl: project.avatar_url,
    text: project.description,
    title: project.name,
    url: project.web_url,
  }
}

export async function fetchGroupProjects(source, options = {}) {
  const { url: baseUrl, group } = source
  const {
    limit = -1, // means no limit in GitLab API
    orderBy = "name",
  } = options
  const url = `${baseUrl}/api/v4/groups/${encodeURIComponent(group)}/projects?per_page=${limit}&order_by=${orderBy}&sort=asc`
  const info = { url, source }
  let response
  try {
    response = await this.fetch(url)
    if (!response.ok) {
      throw response
    }
  } catch (error) {
    throw new FetchError({
      message: `Error while fetching GitLab group "${group}"`,
      info,
    }).withCause(error)
  }
  let projects
  try {
    projects = await response.json()
  } catch (error) {
    throw new FetchError({
      message: `Error while parsing JSON of GitLab group "${group}"`,
      info,
    }).withCause(error)
  }
  return projects
}
